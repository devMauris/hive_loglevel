import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.io.*;

/**
 * Created by mauris on 30/10/15.
 */

@Description(
        name = "level",
        value = "_FUNC_(str) - returns log level"
)
public class HiveUdfLevel extends UDF
{
    public IntWritable evaluate (Text s)
    {
        int code = 0;
        if(s.toString().contains("INFO"))
            code = 1;
        if(s.toString().contains("WARN"))
            code = 2;
        if(s.toString().contains("ERROR"))
            code = 3;
        if(s.toString().contains("FATAL"))
            code = 4;
        return new IntWritable(code);
    }
}
