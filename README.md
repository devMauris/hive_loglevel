Hive Log Level UDF
======
Returns log string level ('INFO' -> 1, 'WARN' -> 2)
